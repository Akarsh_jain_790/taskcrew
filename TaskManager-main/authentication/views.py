from authentication.models import User
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated

from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.views import TokenObtainPairView

from .serializers import UserSerializer, RegisterUserSerializer
from rest_framework import generics


class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)

        # Add custom claims
        token['username'] = user.username
        # ...

        return token


class MyTokenObtainPairView(TokenObtainPairView):
    serializer_class = MyTokenObtainPairSerializer


class UserCreateAPIView(generics.CreateAPIView):
    queryset = User.objects.all()
    serializer_class = RegisterUserSerializer

@api_view(['GET'])
def getRoutes(request):
    routes = [
        '/auth/token',
        '/auth/token/refresh',
    ]

    return Response(routes)

@api_view(['GET'])
@permission_classes([IsAuthenticated])
def current_user(request):
    try:
        obj = User.objects.get(username=request.user)
    except:	
        return Response(
            {'message': 'You are not authorizated'},
            status=status.HTTP_403_FORBIDDEN
        )
    serializer = UserSerializer(obj)
    return Response(serializer.data)

@api_view(['GET'])
@permission_classes([IsAuthenticated])
def userList(request):
    userobj = User.objects.get(username=request.user)
    if userobj.role != "ADMIN":			
        return Response(
            {'message': 'You are not authorizated'},
            status=status.HTTP_403_FORBIDDEN
        )
    try:
        obj = User.objects.all()
    except:	
        return Response(
            {'message': 'You are not authorizated'},
            status=status.HTTP_403_FORBIDDEN
        )
    serializer = UserSerializer(obj, many=True)
    return Response(serializer.data)

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def userCreate(request):
    userobj = User.objects.get(username=request.user)
    if userobj.role != "ADMIN":			
        return Response(
            {'message': 'You are not authorizated'},
            status=status.HTTP_403_FORBIDDEN
        )

    serialized = RegisterUserSerializer(data=request.data)
    if serialized.is_valid():
        User.objects.create_user(
            serialized.init_data['email'],
            serialized.init_data['username'],
            serialized.init_data['role'],
            serialized.init_data['password']
        )
        return Response(serialized.data, status=status.HTTP_201_CREATED)
    else:
        return Response(serialized._errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['PUT'])
@permission_classes([IsAuthenticated])
def userUpdate(request, pk):
    userobj = User.objects.get(username=request.user)
    if userobj.role != "ADMIN":			
        return Response(
            {'message': 'You are not authorizated'},
            status=status.HTTP_403_FORBIDDEN
        )
    user = User.objects.get(id=pk)
    serializer = UserSerializer(instance=user, data=request.data, partial=True)

    if serializer.is_valid():
        serializer.save()

    return Response(serializer.data)


@api_view(['DELETE'])
@permission_classes([IsAuthenticated])
def userDelete(request, pk):
    userobj = User.objects.get(username=request.user)
    if userobj.role != "ADMIN":			
        return Response(
            {'message': 'You are not authorizated'},
            status=status.HTTP_403_FORBIDDEN
        )
    user = User.objects.get(id=pk)
    user.delete()

    return Response('User succsesfully delete!')    
