from django.db import models
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)
from django.utils import timezone

class UserManager(BaseUserManager):
    def _create_user(self, username, email, role, password, is_active, is_staff, is_superuser, **other_fields):
        """
        Creates and saves a user with the given username, email 
        and password.
        """
        date_joined = timezone.now(); 
        if not username:
            raise ValueError('User must have an email address')

        user = self.model(
            username = username,
            email = self.normalize_email(email),
            role = role,
            is_active = is_active,
            is_staff = is_staff,
            is_superuser = is_superuser,
            date_joined = date_joined,
            **other_fields
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username, email, role, password, **other_fields):
        """
        Creates and saves a dependent with the given username, email 
        and password.
        """
        return self._create_user(
            username,
            email,
            role,
            password,
            is_active=True,
            is_staff=False,
            is_superuser=False,
            **other_fields
        )

    def create_superuser(self, username, email, role, password, **other_fields):
        """
        Creates and saves a admin with the given username, email 
        and password.
        """
        user = self._create_user(
            username,
            email,
            role,
            password,
            is_active=True,
            is_staff=True,
            is_superuser=True,
            **other_fields
        )
        user.save(using=self._db)
        return user


class User(AbstractBaseUser):
    username = models.CharField(max_length=30, unique=True)
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
    )

    username = models.CharField(max_length=30, unique=True)
    email = models.EmailField(max_length=250, unique=True)
    first_name = models.CharField(max_length=30, blank=True, null=True)
    last_name = models.CharField(max_length=30, blank=True, null=True)
    is_active = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    date_joined = models.DateTimeField(default=timezone.now)
    
    ROLE_OPTIONS = (
        ("DEPENDENT", "DEPENDENT"),
        ("PARENT", "PARENT"),
        ("ADMIN", "ADMIN")
    )

    role = models.CharField(max_length=30, choices=ROLE_OPTIONS, default="DEPENDENT")

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email','role']
