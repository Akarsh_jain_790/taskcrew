from django.urls import path
from . import views
from .views import MyTokenObtainPairView, UserCreateAPIView
from rest_framework_simplejwt.views import (
    TokenRefreshView,
)
urlpatterns = [
    path('token/', MyTokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('user-userdetails/', views.current_user, name='current_user'),
    path('user-list/', views.userList, name='user_list'),
    path('user-create/', UserCreateAPIView.as_view(), name='user_create'),
    path('user-update/<str:pk>/', views.userUpdate, name='user-update'),
    path('user-delete/<str:pk>/', views.userDelete, name='user-delete')
]
