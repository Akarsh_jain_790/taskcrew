from rest_framework.serializers import ModelSerializer
from rest_framework import serializers
from .models import Tasks


class TaskSerializer(ModelSerializer):
    deadline = serializers.DateTimeField(format="%d-%m-%Y %H:%M:%S")
    created = serializers.DateTimeField(format="%d-%m-%Y %H:%M:%S")

    class Meta:
        model = Tasks
        fields = '__all__'