from authentication.models import User
from django.db import models

class Tasks(models.Model):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True, blank=True)
    title = models.CharField(max_length=200)
    description = models.TextField(null=True, blank=True)
    deadline = models.DateTimeField()
    approver = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True, blank=True, related_name='approver')
    created = models.DateTimeField(auto_now_add=True)
    timetaken = models.DurationField(null=True, blank=True)
    taskstatus = models.BooleanField(default=False)

    def __str__(self):
        return self.title

    class Meta:
        order_with_respect_to = 'user'

