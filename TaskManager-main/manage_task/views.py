import datetime
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework import status

from authentication.models import User
from authentication.serializers import UserSerializer

from .serializers import TaskSerializer
from .models import Tasks
from datetime import timezone

@api_view(['GET'])
def apiOverview(request):
	api_urls = {
		'List':'/task-list/',
		'Detail View':'/task-detail/<str:pk>/',
		'Create':'/task-create/',
		'Update':'/task-update/<str:pk>/',
		'Delete':'/task-delete/<str:pk>/',
		}

	return Response(api_urls)

@api_view(['GET'])
@permission_classes([IsAuthenticated])
def taskList(request):
	user = request.user
	tasks = Tasks.objects.all().filter(user=user).order_by('-id')
	# tasks = Tasks.objects.all().filter(user=request.user.id).order_by('-id')
	serializer = TaskSerializer(tasks, many=True)
	return Response(serializer.data)
	

@api_view(['GET'])
@permission_classes([IsAuthenticated])
def taskDetail(request, pk):
	tasks = Tasks.objects.get(id=pk)
	serializer = TaskSerializer(tasks, many=False)
	return Response(serializer.data)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def taskCreate(request):
	data = request.data
	data['user'] = request.user.id
	serializer = TaskSerializer(data=data,partial=True)

	if serializer.is_valid():
		serializer.save()

	return Response(serializer.data)

@api_view(['PUT'])
@permission_classes([IsAuthenticated])
def taskUpdate(request, pk):
	task = Tasks.objects.get(id=pk)
	serializer = TaskSerializer(instance=task, data=request.data, partial=True)

	if serializer.is_valid():
		serializer.save()

	return Response(serializer.data)


@api_view(['DELETE'])
@permission_classes([IsAuthenticated])
def taskDelete(request, pk):
	task = Tasks.objects.get(id=pk)
	task.delete()

	return Response('Item succsesfully delete!')

# Parent Endpoints
@api_view(['GET'])
@permission_classes([IsAuthenticated])
def dependentTaskList(request):
	try:
		obj = User.objects.get(role="DEPENDENT")
	except:
		print(request.user.id)
		return Response(
			status=status.HTTP_404_NOT_FOUND
		)
	user_role = User.objects.get(username= request.user)
	if user_role.role != "PARENT":			
		return Response(
			{'message': 'You are not authorizated'},
			status=status.HTTP_403_FORBIDDEN
		)
	tasks = Tasks.objects.filter	(user=obj.id)
	serializer = TaskSerializer(tasks, many=True)
	return Response(serializer.data)

@api_view(['PUT'])
@permission_classes([IsAuthenticated])
def taskApprove(request, pk):
	user_role = User.objects.get(username= request.user)
	if user_role.role == "PARENT" or user_role.role == "ADMIN":			
		data = request.data
		data['approver'] = request.user.id

		taskcreated = Tasks.objects.filter(id=pk)[0]
		duration = datetime.datetime.now(timezone.utc) - taskcreated.created   
		if (duration.seconds//60)%60  < 120:
			data['timetaken'] = duration

		task = Tasks.objects.get(id=pk)
		serializer = TaskSerializer(instance=task, data=data, partial=True)

		if serializer.is_valid():
			serializer.save()

		return Response(serializer.data)
	else:
		return Response(
			{'message': 'You are not authorizated'},
			status=status.HTTP_403_FORBIDDEN
		)

# Family Admin Endpoints

@api_view(['GET'])
@permission_classes([IsAuthenticated])
def usersTaskList(request):
	obj = User.objects.get(username=request.user)
	if obj.role != "ADMIN":			
		return Response(
			{'message': 'You are not authorizated'},
			status=status.HTTP_403_FORBIDDEN
		)

	tasks = Tasks.objects.all().order_by('-user')
	serializer = TaskSerializer(tasks, many=True)
	return Response(serializer.data)

@api_view(['GET'])
@permission_classes([IsAuthenticated])
def userList(request):
	obj = User.objects.get(username=request.user)
	if obj.role != "ADMIN":			
		return Response(
			{'message': 'You are not authorizated'},
			status=status.HTTP_403_FORBIDDEN
		)
	users = User.objects.all()
	serializer = UserSerializer(users, many=True)
	return Response(serializer.data)
