from django.template.loader import render_to_string
from django.core.mail import EmailMultiAlternatives
from smtplib import SMTPException
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view
from celery import shared_task

@api_view(('GET',))
@shared_task
def send_reminder(request):
    emailSubject = "Subject"
    emailOfSender = "assignmentinternship10@gmail.com"
    emailOfRecipient = ''

    context = ({"name": ""}) 

    html_content = render_to_string('reminder.html', context, request=request)

    try:
        #I used EmailMultiAlternatives because I wanted to send both text and html
        emailMessage = EmailMultiAlternatives(subject=emailSubject, body="text_content", from_email=emailOfSender, to=[emailOfRecipient,], reply_to=[emailOfSender,])
        emailMessage.attach_alternative(html_content, "text/html")
        emailMessage.send(fail_silently=False)
        return Response(
			{'message': 'Reminder Send Successfully'},
			status=status.HTTP_200_OK
		)
    except SMTPException as e:
        print('There was an error sending an email: ', e) 
        error = {'message': ",".join(e.args) if len(e.args) > 0 else 'Unknown Error'}
        raise serializers.ValidationError(error)