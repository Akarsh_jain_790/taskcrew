from django.apps import AppConfig


class TaskcrewConfig(AppConfig):
    name = 'taskcrew'
