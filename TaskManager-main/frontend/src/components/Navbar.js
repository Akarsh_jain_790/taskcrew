import React,{useEffect,useContext} from "react";
import { Link, useLocation} from "react-router-dom";
import AuthContext from '../context/AuthContext'
import TaskContext from '../context/TaskContext'
 
const Navbar = () => {
  let {user, logoutUser} = useContext(AuthContext)
  let {userDetails} = useContext(TaskContext)
  let location = useLocation();
  useEffect(() => {
     
  }, []);

  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <div className="container-fluid">
          <Link className="navbar-brand" to="/">
            TaskCrew
          </Link>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          
         <div className="collapse navbar-collapse" id="navbarSupportedContent">
            {user ? <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              {(userDetails.role==="PARENT" || userDetails.role==="DEPENDENT")?<li className="nav-item">
                <Link className={`nav-link ${location.pathname==="/"?"active":""}`} aria-current="page" to="/">
                  My Tasks
                </Link>
              </li>:<></>}
              {(userDetails.role==="PARENT" || userDetails.role==="DEPENDENT")?<li className="nav-item">
                <Link className={`nav-link ${location.pathname==="/create-task/"?"active":""}`} aria-current="page" to="/CreateTask/">
                  Create Task
                </Link>
              </li>:<></>}
              {(userDetails.role==="PARENT")?<li className="nav-item">
                <Link className={`nav-link ${location.pathname==="/dependent-tasks/"?"active":""}`} to="/DependentTask/">
                  Dependent Tasks
                </Link>
              </li>:<></>}
              {(userDetails.role==="ADMIN")?<li className="nav-item">
                <Link className={`nav-link ${location.pathname==="/all-tasks/"?"active":""}`} to="/">
                  All Tasks
                </Link>
              </li>:<></>}
              {(userDetails.role==="ADMIN")?<li className="nav-item">
                <Link className={`nav-link ${location.pathname==="/manage-users/"?"active":""}`} to="/ManageUser/">
                  Manage users
                </Link>
              </li>:<></>}
            </ul>: <></>}
            {user ? (
              <div className="d-flex text-center">
                <p className="text-light">Hello {user.username}</p>
                <button className="btn btn-dark" onClick={logoutUser}>Logout</button>   
              </div>
            ): (
                <></>
            )}
           
          </div>
        </div>
      </nav>
    </>
  );
};

export default Navbar;
