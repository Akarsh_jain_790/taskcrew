import React, {useState, useEffect, useRef, useContext} from 'react'
import EditUser from '../components/EditUser';
import AuthContext from '../context/AuthContext'
import { Link } from "react-router-dom";

const ManageUser=() =>{

const context = useContext(AuthContext);

const {alluser, getAllUsers, deleteUser} = context;
const [user, setUser] = useState({id:"",eusername:"",eemail:"",erole:"",epassword:""})
const updateUser = (currentUser) => {
    setUser({id:currentUser.id,eusername:currentUser.username,eemail:currentUser.email,erole:currentUser.role,epassword:currentUser.password})
    
};
useEffect(() => {
    
    getAllUsers();

}, []);

return (
    <div>
        <EditUser updateUser={updateUser} user={user} setUser={setUser}/>
        <section className="vh-100" style={{backgroundColor: '#eee'}}>
                <div className="container py-5 h-10">
                <div className="row d-flex justify-content-center align-items-center h-100">
                    <div className="col-md-12 col-xl-10">
                    <div className="card">
                        <div className="card-header p-3">
                        <h5 className="mb-0"><i className="fas fa-tasks me-2" />User List</h5>
                        <div className="d-flex justify-content-end">
                            <h5 className="p-1">
                                <Link className="badge bg-success" aria-current="page" to="/AddUser/">
                                Add User
                                </Link>
                            </h5>
                            </div>
                        </div>
                        <div className="card-body" data-mdb-perfect-scrollbar="true" data-spy="scroll" data-offset="2"tyle={{position: 'relative', height: '400px'}}>
                        <table className="table mb-0" >
                            <thead>
                            <tr>
                                <th scope="col">UserName</th>
                                <th scope="col">Email</th>
                                <th scope="col">Role</th>
                                <th scope="col">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                                {alluser.length===0 && 
                                <tr className="fw-normal">
                                    <th>
                                    <span className="ms-2">No User Found</span>
                                    </th>
                                </tr>
                                }
                                {alluser.map((user) => {
                                return (
                                    <tr className="fw-normal" key={user.id}>
                                    <th  data-bs-toggle="modal">
                                    <span className="ms-2">{user.username}</span>
                                    </th>
                                    <td className="align-middle">
                                    <span>{user.email}</span>
                                    </td>
                                    <td className="align-middle"> 
                                    <h6 className="mb-0"><span className="badge bg-danger">{user.role}</span></h6>
                                    </td>
                                    <td className="align-middle">
                                    <a href="#!" data-mdb-toggle="tooltip" title="Edit"><i className="fas fa-edit text-info me-3" data-bs-toggle="modal" data-bs-target="#EditModal" onClick={() =>{updateUser(user)}}/></a>
                                    <a href="#!" data-mdb-toggle="tooltip" title="Remove"><i className="fas fa-trash-alt text-danger" onClick={() =>{deleteUser(user.id);}} /></a>
                                    </td>
                                </tr>
                                );
                                })}
                            </tbody>
                        </table>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
            </section>
    </div>
)

}

export default ManageUser
