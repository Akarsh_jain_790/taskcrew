import React, {useRef, useContext} from 'react'
import AuthContext from '../context/AuthContext'
function EditUser(props) {
    const context = useContext(AuthContext);
    const { editUser } = context;

    const handleClick = (e) =>{
        editUser(props.user.id,props.user.eusername,props.user.eemail,props.user.erole,props.user.epassword);
        refClose.current.click();
    }

    const onChange = (e) =>{
        props.setUser({...props.user,[e.target.name]:e.target.value})
    }
    
  const refClose = useRef(null);
  return (
    <div>
      <div
                className="modal fade"
                id="EditModal"
                tabIndex="-1"
                aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header">
                    <h5 className="modal-title" id="exampleModalLabel">
                        Edit User
                    </h5>
                    <button
                        type="button"
                        className="btn-close"
                        data-bs-dismiss="modal"
                        aria-label="Close"
                    ></button>
                    </div>
                    <div className="modal-body">
                    <form className="my-3">
                        <div className="mb-3">
                        <label htmlFor="etitle" className="form-label">
                            Username
                        </label>
                        <input
                            type="text"
                            className="form-control"
                            id="eusername"
                            name="eusername"
                            value={props.user.eusername}
                            onChange={onChange}
                            minLength={5}
                            required
                        />
                        </div>
                        <div className="mb-3">
                        <label htmlFor="edescription" className="form-label">
                            Email
                        </label>
                        <input
                            type="email"
                            className="form-control"
                            name="eemail"
                            id="eemail"
                            value={props.user.eemail}
                            onChange={onChange}
                            minLength={5}
                            required
                        />
                        </div>
                        <div className="mb-3">
                        <label htmlFor="erole" className="form-label">
                            Role
                        </label>
                        <select id="erole" value={props.user.role} name="erole" onChange={onChange} required>
                            <option value="Select Role:-">Select Role:-</option>
                            <option value="ADMIN">ADMIN</option>
                            <option value="PARENT">PARENT</option>
                            <option value="DEPENDENT">DEPENDENT</option>
                        </select>
                        </div>
                        <div className="mb-3">
                        <label htmlFor="epassword" className="form-label">
                            Password
                        </label>
                        <input
                            type="password"
                            className="form-control"
                            name="epassword"
                            id="epassword"
                            value={props.user.epassword}
                            onChange={onChange}
                            minLength={8}
                            required
                        />
                        </div>
                    </form>
                    </div>
                    <div className="modal-footer">
                    <button
                        type="button"
                        ref={refClose}
                        className="btn btn-secondary"
                        data-bs-dismiss="modal"
                    >
                        Close
                    </button>
                    <button onClick={handleClick} type="button" className="btn btn-primary">
                        Update User
                    </button>
                    </div>
                </div>
                </div>
            </div>
    </div>
  );
}

export default EditUser
