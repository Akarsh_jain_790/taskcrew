import React, {useEffect, useContext} from 'react'
import { Link,useHistory} from "react-router-dom";
import TaskContext from '../context/TaskContext'

const ApprovedTasks=(props) =>{

const context = useContext(TaskContext);
const history = useHistory();
const { tasks, getDependentTask,userDetails} = context;

useEffect(() => {
    if(userDetails.role==='PARENT'){
        getDependentTask();

    } else {
        history.push('/')
    }

}, []);

return (
    <div>
        <section className="vh-100" style={{backgroundColor: '#eee'}}>
        <div className="container py-5 h-10">
          <div className="row d-flex justify-content-center align-items-center h-100">
            <div className="col-md-12 col-xl-10">
              <div className="card">
              <div className="card-header p-3 d-flex justify-content-between">
                  <h5 className="mb-0 align-right"><i className="fas fa-tasks me-2" />Task List</h5>
                <div className="d-flex justify-content-end">
                      {(userDetails.role==="PARENT")?<h5 className="p-1">
                        <Link className="badge bg-success" aria-current="page" to="/DependentTask/">
                          Dependent Tasks
                        </Link>
                      </h5>:<></>}
                      {(userDetails.role==="PARENT")?<h5 className="p-1">
                        <Link className="badge bg-success" aria-current="page" to="/ApprovedTask/">
                          Approved Task
                        </Link>
                      </h5>:<></>}
                    </div>
                    </div>
                <div/>
                <div className="card-body" data-mdb-perfect-scrollbar="true" data-spy="scroll" data-offset="2"tyle={{position: 'relative', height: '400px'}}>
                  <table className="table mb-0" >
                    <thead>
                      <tr>
                        <th scope="col">User Name</th>
                        <th scope="col">Task</th>
                        <th scope="col">Status</th>
                        <th scope="col">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                        {tasks.length===0 && 
                        <tr className="fw-normal">
                            <th>
                            <span className="ms-2">No task to display</span>
                            </th>
                        </tr>
                        }
                        {tasks.map((task) => {
                        return (
      
                            (task.approver===userDetails.id) &&  
                              <tr className="fw-normal">
                            <th>
                              <span className="ms-2">{task.user}</span>
                            </th>
                            <td className="align-middle">
                              <span>{task.title}</span>
                            </td>
                            <td className="align-middle">
                              <h6 className="mb-0">{(task.taskstatus === true)?<span className="badge bg-success">Completed</span>:<span className="badge bg-warning">Pending</span>}</h6>
                            </td>
                            <div className="py-2 px-3 me-2 border border-success rounded-3 d-flex align-items-center bg-light">
                              <p className="small mb-0">
                              <a href="#!" data-mdb-toggle="tooltip" title="Time Taken">
                                  <i className="fas fa-hourglass-half me-2 text-success" />
                              </a>
                              {task.timetaken}
                              </p>
                          </div>
                            <td className="align-middle">
                              <a href="#!" data-mdb-toggle="tooltip" title="Done"><i className="fas fa-tasks text-success me-3 text-success me-3" /></a>
                            </td>
                          </tr>
                        )
                        })}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
)

}

export default ApprovedTasks

