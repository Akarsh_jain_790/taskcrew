import React, {useState, useEffect, useContext} from 'react'
import TaskItem from './TaskItem';
import TaskDetail from '../pages/TaskDetail';
import EditTask from '../components/EditTask';
import TaskContext from '../context/TaskContext'
import '../App.css';

const Task=() =>{
    const context = useContext(TaskContext);

    const { tasks, getTask } = context;

    useEffect(() => {
        
        getTask();

    }, []);

    const [task, setTask] = useState({id:"",etitle:"",edescription:"",edeadline:"",edeadline:""})
    const updateTask = (currentTask) => {
        setTask({id:currentTask.id,etitle:currentTask.title,edescription:currentTask.description})
        
    };
    const showTask = (currentTask) => {
        setTask({id:currentTask.id,etitle:currentTask.title,edescription:currentTask.description,edeadline:currentTask.deadline,ecreated:currentTask.created})
        
    };
    return (
        <div>
            <TaskDetail showTask={showTask} task={task} />
            <EditTask updateTask={updateTask} task={task} setTask={setTask}/>
        <section className="vh-10">
        <div className="container py-5 h-100">
          <div className="row d-flex justify-content-center align-items-center h-100">
            <div className="col">
              <div className="card" id="list1" style={{borderRadius: '.75rem', backgroundColor: '#eff1f2'}}>
                <div className="card-body py-4 px-4 px-md-5">
                  <p className="h1 text-center mt-3 mb-4 pb-3 text-primary">
                    <u>TaskCrew</u>
                  </p>
                  <hr className="my-4" />
                    {tasks.length===0 && 
                        <ul className="list-group list-group-horizontal rounded-0">
                            <li className="list-group-item px-3 py-1 d-flex align-items-center flex-grow-1 border-0 bg-transparent">
                            <p className="lead fw-normal mb-0">No task to display</p>
                            </li>
                        </ul>
                    }
                    {tasks.map((task) => {
                        return (
                            <TaskItem key={task.id} updateTask={updateTask} showTask={showTask} task={task} />
                        );
                    })}
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
        </div>
    )
}

export default Task
