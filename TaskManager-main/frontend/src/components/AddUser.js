import React, { useState,useContext } from "react";
import AuthContext from '../context/AuthContext'
import { useHistory } from 'react-router-dom'

const AddUser = () => {
  const context = useContext(AuthContext);
  const { addUser } = context;
  const history = useHistory()

  const [user, setUser] = useState({"username":"","email":"","role":"","password":""})

  const handleClick = (e) =>{ 
    addUser(user.username,user.email,user.role,user.password);
    history.push('/')
  }
  const onChange = (e) =>{
      setUser({...user,[e.target.name]:e.target.value})
  }
  return (
    <>
      <div className="container my-3">
        <h1>Add User</h1>
        <form className="my-3">
          <div className="mb-3">
          <label htmlFor="username" className="form-label">
              Username
          </label>
          <input
              type="text"
              className="form-control"
              id="username"
              name="username"
              value={user.username}
              onChange={onChange}
              minLength={5}
              required
          />
          </div>
          <div className="mb-3">
          <label htmlFor="email" className="form-label">
              Email
          </label>
          <input
              type="email"
              className="form-control"
              name="email"
              id="email"
              pattern="/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/"
              value={user.email}
              onChange={onChange}
              required
          />
          </div>
          <div className="mb-3">
          <label htmlFor="role" className="form-label">
              Role
          </label>
          <select id="role" value={user.role} name="role" onChange={onChange} required>
              <option value="Select Role:-">Select Role:-</option>
              <option value="ADMIN">ADMIN</option>
              <option value="PARENT">PARENT</option>
              <option value="DEPENDENT">DEPENDENT</option>
          </select>
          </div>
          <div className="mb-3">
          <label htmlFor="password" className="form-label">
              Password
          </label>
          <input
              type="password"
              className="form-control"
              name="password"
              id="password"
              value={user.password}
              onChange={onChange}
              minLength={8}
              required
          />
          </div>
      </form>
          <button disabled={user.username.length<5 || user.password.length<8 || !user.role } type="submit" className="btn btn-primary" onClick={handleClick}>
            Add User
          </button>
      </div>
    </>
  );
};

export default AddUser;
