import React, {useRef, useContext} from 'react'
import TaskContext from '../context/TaskContext'

function EditTask(props) {
    const context = useContext(TaskContext);
    const { editTask } = context;

    const handleClick = (e) =>{
        editTask(props.task.id,props.task.etitle,props.task.edescription);
        refClose.current.click();
    }

    const onChange = (e) =>{
        props.setTask({...props.task,[e.target.name]:e.target.value})
    }
    
  const refClose = useRef(null);
  return (
    <div>
      <div
                className="modal fade"
                id="EditModal"
                tabIndex="-1"
                aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header">
                    <h5 className="modal-title" id="exampleModalLabel">
                        Edit task
                    </h5>
                    <button
                        type="button"
                        className="btn-close"
                        data-bs-dismiss="modal"
                        aria-label="Close"
                    ></button>
                    </div>
                    <div className="modal-body">
                    <form className="my-3">
                        <div className="mb-3">
                        <label htmlFor="etitle" className="form-label">
                            Title
                        </label>
                        <input
                            type="text"
                            className="form-control"
                            id="etitle"
                            name="etitle"
                            value={props.task.etitle}
                            aria-describedby="emailHelp"
                            onChange={onChange}
                            minLength={5}
                            required
                        />
                        </div>
                        <div className="mb-3">
                        <label htmlFor="edescription" className="form-label">
                            Description
                        </label>
                        <input
                            type="text"
                            className="form-control"
                            name="edescription"
                            id="edescription"
                            value={props.task.edescription}
                            onChange={onChange}
                            minLength={5}
                            required
                        />
                        </div>
                    </form>
                    </div>
                    <div className="modal-footer">
                    <button
                        type="button"
                        ref={refClose}
                        className="btn btn-secondary"
                        data-bs-dismiss="modal"
                    >
                        Close
                    </button>
                    <button disabled={props.task.etitle.length<5 || props.task.edescription.length<5} onClick={handleClick} type="button" className="btn btn-primary">
                        Update Task
                    </button>
                    </div>
                </div>
                </div>
            </div>
    </div>
  );
}

export default EditTask;
