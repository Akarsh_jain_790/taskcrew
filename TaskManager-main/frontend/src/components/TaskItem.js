import React,{useContext} from "react";
import TaskContext from "../context/TaskContext";

const TaskItem = (props) => {
  const context = useContext(TaskContext);
  const { deleteTask, userDetails, approveTask } = context;
  const { task , updateTask ,showTask} = props;
  return (
    <ul className="list-group list-group-horizontal rounded-0">
    <li className="list-group-item px-3 py-1 d-flex align-items-center flex-grow-1 border-0 bg-transparent" data-bs-toggle="modal" data-bs-target="#DetailModal" onClick={() =>{showTask(task)}}>
    <p className="lead fw-normal mb-0">{task.title}</p>
    </li>
    <li className="list-group-item px-3 py-1 d-flex align-items-center border-0 bg-transparent" data-bs-toggle="modal" data-bs-target="#DetailModal" onClick={() =>{showTask(task)}}>
    <div className="py-2 px-3 me-2 border border-warning rounded-3 d-flex align-items-center bg-light">
        <p className="small mb-0">
        <a href="#!" data-mdb-toggle="tooltip" title="Due on date">
            <i className="fas fa-hourglass-half me-2 text-warning" />
        </a>
        {task.deadline}
        </p>
    </div>
    <div className="py-2 px-3 me-2 border border-success rounded-3 d-flex align-items-center bg-light">
        <p className="small mb-0">
        <a href="#!" data-mdb-toggle="tooltip" title="Time Taken">
            <i className="fas fa-hourglass-half me-2 text-success" />
        </a>
        {task.timetaken}
        </p>
    </div>
    </li>
    <li className="list-group-item ps-3 pe-0 py-1 rounded-0 border-0 bg-transparent">
    <div className="d-flex flex-row justify-content-end mb-1">
        {(userDetails.role === "PARENT")?
        <a href="#!" data-mdb-toggle="tooltip" title="Done"><i className="fas fa-check text-success me-3" onClick={() =>{approveTask(task.id,true);}} /></a>
        :<></>}
        <a href="#!" className="text-info" data-mdb-toggle="tooltip" title="Edit todo"><i className="fas fa-pencil-alt me-3" data-bs-toggle="modal" data-bs-target="#EditModal"onClick={() =>{updateTask(task)}}/></a>
        <a href="#!" className="text-danger" data-mdb-toggle="tooltip" title="Delete todo"><i className="fas fa-trash-alt" onClick={() =>{deleteTask(task.id); props.showAlert("Deleted succesfully","success")}} /></a>
    </div>
    <div className="text-end text-muted">
        <a href="#!" className="text-muted" data-mdb-toggle="tooltip" title="Created date">
        <p className="small mb-0"><i className="fas fa-info-circle me-2" />{task.created}</p></a>
    </div>
    </li>
</ul>


  );
};

export default TaskItem;
