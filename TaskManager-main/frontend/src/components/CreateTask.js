import React, { useState,useContext } from "react";
import TaskContext from "../context/TaskContext";
import { useHistory } from 'react-router-dom'

const CreateTask = () => {
  const context = useContext(TaskContext);
  const history = useHistory()

  const { addTask } = context;
  const [task, settask] = useState({"title":"","description":"","deadline":""})
  const handleClick = (e) =>{
    e.preventDefault();

    var dateControl = document.querySelector('input[type="datetime-local"]');
    addTask(task.title,task.description,dateControl.value);
    settask({"title":"","description":"","deadline":""})
    history.push('/')
  }
  const onChange = (e) =>{
    settask({...task,[e.target.name]:e.target.value})
  }
  return (
    <>
      <div className="container my-3">
        <h1>Add a Task</h1>
        <form className="my-3">
          <div className="mb-3">
            <label htmlFor="title" className="form-label">
              Title
            </label>
            <input
              type="text"
              className="form-control"
              id="title"
              name="title"
              value={task.title}
              aria-describedby="emailHelp"
              onChange={onChange}
              minLength={5}
              required
            />
          </div>
          <div className="mb-3">
            <label htmlFor="description" className="form-label">
              Description
            </label>
            <input
              type="text"
              className="form-control"
              name="description"
              id="description"
              value={task.description}
              onChange={onChange}
              minLength={5}
              required
            />
          </div>
            <div className="mb-3">
            <label htmlFor="edeadline" className="form-label">
                Deadline
            </label>
            <input
                type="datetime-local"
                className="form-control"
                name="edeadline"
                id="edeadline"
                value={task.edeadline}
                onChange={onChange}
            />
            </div>
          <button disabled={task.title.length<5 || task.description.length<5} type="submit" className="btn btn-primary" onClick={handleClick}>
            Add Task
          </button>
        </form>
      </div>
    </>
  );
};

export default CreateTask;
