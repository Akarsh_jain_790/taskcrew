import React, {useState, useEffect, useContext} from 'react'
import { useHistory } from 'react-router-dom';
import TaskContext from '../context/TaskContext'
import TaskDetail from '../pages/TaskDetail';
import EditTask from '../components/EditTask';

const AllTask=() =>{

const context = useContext(TaskContext);
const history = useHistory();
const { tasks, getAllTask,userDetails, deleteTask, approveTask} = context;
const [task, setTask] = useState({id:"",etitle:"",edescription:"",edeadline:"",edeadline:""})

const updateTask = (currentTask) => {
    setTask({id:currentTask.id,etitle:currentTask.title,edescription:currentTask.description})
    
};
const showTask = (currentTask) => {
    setTask({id:currentTask.id,etitle:currentTask.title,edescription:currentTask.description,edeadline:currentTask.deadline,ecreated:currentTask.created})
    
};

useEffect(() => {
    if(userDetails.role==='ADMIN'){
        getAllTask();

    } else {
        history.push('/')
    }

}, []);

return (
    <div>
        <TaskDetail showTask={showTask} task={task} />
        <EditTask updateTask={updateTask} task={task} setTask={setTask} />

        <section className="vh-100" style={{backgroundColor: '#eee'}}>
        <div className="container py-5 h-10">
          <div className="row d-flex justify-content-center align-items-center h-100">
            <div className="col-md-12 col-xl-10">
              <div className="card">
                <div className="card-header p-3">
                  <h5 className="mb-0"><i className="fas fa-tasks me-2" />Task List</h5>
                </div>
                <div className="card-body" data-mdb-perfect-scrollbar="true" data-spy="scroll" data-offset="2"tyle={{position: 'relative', height: '400px'}}>
                  <table className="table mb-0" >
                    <thead>
                      <tr>
                        <th scope="col">User Name</th>
                        <th scope="col">Task</th>
                        <th scope="col">Status</th>
                        <th scope="col">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                        {tasks.length===0 && 
                        <tr className="fw-normal">
                            <th>
                            <span className="ms-2">No task to display</span>
                            </th>
                        </tr>
                        }
                        {tasks.map((task) => {
                        return (
                            <tr className="fw-normal" key={task.id}>
                            <th data-bs-toggle="modal" data-bs-target="#DetailModal" onClick={() =>{showTask(task)}}>
                              <span className="ms-2">{task.user}</span>
                            </th>
                            <td className="align-middle" data-bs-toggle="modal" data-bs-target="#DetailModal" onClick={() =>{showTask(task)}}>
                              <span>{task.title}</span>
                            </td>
                            <td className="align-middle" data-bs-toggle="modal" data-bs-target="#DetailModal" onClick={() =>{showTask(task)}}>
                              <h6 className="mb-0">{(task.taskstatus === true)?<span className="badge bg-success">Completed</span>:<span className="badge bg-warning">Pending</span>}</h6>
                            </td>
                            <td className="align-middle">
                             <a href="#!" data-mdb-toggle="tooltip" title="Done"><i className="fas fa-check text-success me-3" onClick={() =>{approveTask(task.id,true);}} /></a>
                              <a href="#!" data-mdb-toggle="tooltip" title="Edit"><i className="fas fa-edit text-info me-3" data-bs-toggle="modal" data-bs-target="#EditModal"onClick={() =>{updateTask(task)}}/></a>
                              <a href="#!" data-mdb-toggle="tooltip" title="Remove"><i className="fas fa-trash-alt text-danger" onClick={() =>{deleteTask(task.id);}} /></a>
                            </td>
                          </tr>
                        );
                        })}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
)

}

export default AllTask
