import { Route, Redirect } from 'react-router-dom'
import { useContext ,useEffect} from 'react'
import TaskContext from '../context/TaskContext'

const AdminRoute = ({children, ...rest}) => {
    let {userDetails,getUserData} = useContext(TaskContext)
    useEffect(() => {
        getUserData()
        // eslint-disable-next-line
    }, [])
    return(
        <Route {...rest}>{!userDetails.role === "ADMIN" ? <Redirect to="/" /> :   children}</Route>
    )
}

export default AdminRoute;