import './App.css';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import {AuthProvider} from './context/AuthContext';
import CreateTask from "./components/CreateTask";
import AddUser from "./components/AddUser";
import TaskState from "./context/TaskState";
import AllTask from "./components/AllTask";
import DependentTask from "./components/DependentTask";
import ApprovedTasks from './components/ApprovedTasks';

import ManageUser from "./components/ManageUser";
import Navbar from './components/Navbar'

import HomePage from './pages/HomePage';
import LoginPage from './pages/LoginPage';

import PrivateRoute from './utils/PrivateRoute';
import AdminRoute from './utils/AdminRoute';

function App() {

  return (
    <div className="App">
      <Router>       
        <AuthProvider>
          <TaskState>
          <Navbar/>
          <PrivateRoute component={HomePage} path="/" exact/>
          <Route component={LoginPage}  path="/login/"/>
          <PrivateRoute>
          <Route component={CreateTask}  path="/createtask/"/>
          <Route component={DependentTask}  path="/DependentTask/"/>
          <Route component={ApprovedTasks}  path="/ApprovedTask/"/>
          <AdminRoute component={AllTask}  path="/AllTask/"/>
          <AdminRoute component={ManageUser}  path="/manageuser/"/>
          <AdminRoute component={AddUser}  path="/AddUser/"/>
          </PrivateRoute>
         </TaskState>
        </AuthProvider>
      </Router>
    </div>
  );
}

export default App;