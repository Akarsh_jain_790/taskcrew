import { createContext, useState, useEffect, useContext } from 'react'
import jwt_decode from "jwt-decode";
import { useHistory } from 'react-router-dom'
// import AlertContext from './AlertContext';

const AuthContext = createContext()

export default AuthContext;


export const AuthProvider = ({children}) => {
    let [authTokens, setAuthTokens] = useState(()=> localStorage.getItem('authTokens') ? JSON.parse(localStorage.getItem('authTokens')) : null)
    let [user, setUser] = useState(()=> localStorage.getItem('authTokens') ? jwt_decode(localStorage.getItem('authTokens')) : null)
    let [loading, setLoading] = useState(true)
    const allusers = []
    let [alluser, setAllUserData] = useState(allusers)
   
    // let {showAlert} = useContext(AlertContext)
    const history = useHistory()


    let loginUser = async (e )=> {
        e.preventDefault()
        let response = await fetch('http://127.0.0.1:8000/auth/token/', {
            method:'POST',
            headers:{
                'Content-Type':'application/json'
            },
            body:JSON.stringify({'username':e.target.username.value, 'password':e.target.password.value})
        })
        let data = await response.json()

        if(response.status === 200){
            setAuthTokens(data)
            setUser(jwt_decode(data.access))
            localStorage.setItem('authTokens', JSON.stringify(data))
            history.push('/')

        }else{
            // console.log("Unable to login");
        }
    }


    let logoutUser = () => {
        setAuthTokens(null)
        setUser(null)
        localStorage.removeItem('authTokens')
        history.push('/login')
    }


    let updateToken = async ()=> {

        let response = await fetch('http://127.0.0.1:8000/auth/token/refresh/', {
            method:'POST',
            headers:{
                'Content-Type':'application/json'
            },
            body:JSON.stringify({'refresh':authTokens?.refresh})
        })

        let data = await response.json()
        
        if (response.status === 200){
            setAuthTokens(data)
            setUser(jwt_decode(data.access))
            localStorage.setItem('authTokens', JSON.stringify(data))
        }else{
            logoutUser()
        }

        if(loading){
            setLoading(false)
        }
    }

    
    const getAllUsers = async () =>{
        /* 
            Fetching All users from DRF(Django Rest Framework)
        */
        let response = await fetch(`http://127.0.0.1:8000/auth/user-list/`, {
            method:'GET',
            headers:{
                'Content-Type':'application/json',
                'Authorization':'Bearer ' + String(authTokens.access)
            }
        })
        let data = await response.json()

        if(response.status === 200){
            setAllUserData(data)
        }else if(response.statusText === 'Unauthorized'){
            logoutUser()
        }
      }

      const addUser = async (username,email,role,password) =>{
        /* 
            Creating new user
        */
         const response = await fetch(`http://127.0.0.1:8000/auth/user-create/`, {
          method: 'POST', 
          headers: {
            'Content-Type': 'application/json',
            'Authorization':'Bearer ' + String(authTokens.access)
            
          },
          body: JSON.stringify({username,email,role,password}) 
        });
        const user = await response.json();
        
        if(response.status === 201){
            setAllUserData(alluser.concat(user))
        }else {
            logoutUser()
        }
      }
      
      const deleteUser =async (id) =>{
        /* 
            Deleting user
        */
        const response = await fetch(`http://127.0.0.1:8000/auth/user-delete/${id}/`, {
          method: 'DELETE', 
          headers: {
            'Content-Type': 'application/json',
            'Authorization':'Bearer ' + String(authTokens.access)
            
          },
        });
        const user = response.json(); 
        const newUsers = alluser.filter((user)=>{return user.id!==id})
       
        if(response.status === 200){
            setAllUserData(newUsers);
            
        }else{
            logoutUser()
        }
      }

      const editUser = async (id,username,email,role,password) =>{
       /* edit user profile */
        const response = await fetch(`http://127.0.0.1:8000/auth/user-update/${id}/`, {
            method: 'PUT', 
            headers: {
            'Content-Type': 'application/json',
            'Authorization':'Bearer ' + String(authTokens.access)
            
            },
            body: JSON.stringify({username,email,role,password}) 
        });

        //logic to edit in client
        let newUsers = JSON.parse(JSON.stringify(alluser))
        for (let index = 0; index < newUsers.length; index++) {
            const element = newUsers[index];
            if(element.id===id)
            {
                newUsers[index].username=username;
                newUsers[index].email=email;
                newUsers[index].role=role;
            break;
            }
        }
        
        if(response.status === 200){
            setAllUserData(newUsers);
        }else{
            logoutUser()
        }
        }

    let contextData = {
        user:user,
        authTokens:authTokens,
        loginUser:loginUser,
        logoutUser:logoutUser,
        getAllUsers:getAllUsers,
        deleteUser:deleteUser,
        addUser:addUser,
        editUser:editUser,
        alluser:alluser,
    }


    useEffect(()=> {

        if(loading){
            updateToken()
        }

        let fourMinutes = 1000 * 60 * 4

        let interval =  setInterval(()=> {
            if(authTokens){
                updateToken()
            }
        }, fourMinutes)
        return ()=> clearInterval(interval)
// eslint-disable-next-line
    }, [authTokens, loading])

    return(
        <AuthContext.Provider value={contextData} >
            {loading ? null : children}
        </AuthContext.Provider>
    )
}
