import React, {useState, useEffect, useContext} from 'react'
import AuthContext from '../context/AuthContext'
import TaskContext from "./TaskContext";

const TaskState = (props) =>{
    const {authTokens, logoutUser, user} = useContext(AuthContext)

    const host = "http://127.0.0.1:8000"
    const TasksInitial = []
    const Details = []
    const [tasks, setTasks] = useState(TasksInitial)
    const [userDetails, setUserDetails] = useState(Details)
    const getAllTask = async () =>{
      /* 
          Fetching all tasks of user
      */
      let response = await fetch(`${host}/task/users-task-list/`, {
          method:'GET',
          headers:{
              'Content-Type':'application/json',
              'Authorization':'Bearer ' + String(authTokens.access)
          }
      })
      let data = await response.json()

      if(response.status === 200){
          setTasks(data)
      }else if(response.statusText === 'Unauthorized'){
          logoutUser()
      }
    }

    const getDependentTask = async () =>{
      /* 
          Fetching all tasks of dependent 
      */
      let response = await fetch(`${host}/task/dependent-task-list/`, {
          method:'GET',
          headers:{
              'Content-Type':'application/json',
              'Authorization':'Bearer ' + String(authTokens.access)
          }
      })
      let data = await response.json()

      if(response.status === 200){
          setTasks(data)
      }else if(response.statusText === 'Unauthorized'){
          logoutUser()
      }
    }

    const getTask = async () =>{
        /* 
            Fetching tasks of a current user 
        */
        let response = await fetch(`${host}/task/task-list/`, {
            method:'GET',
            headers:{
                'Content-Type':'application/json',
                'Authorization':'Bearer ' + String(authTokens.access)
            }
        })
        let data = await response.json()

        if(response.status === 200){
            setTasks(data)
        }else if(response.statusText === 'Unauthorized'){
            logoutUser()
        }
      }

      const addTask = async (title,description,deadline) =>{
        /* 
            Adding task of user
        */
         const response = await fetch(`${host}/task/task-create/`, {
          method: 'POST', 
          headers: {
            'Content-Type': 'application/json',
            'Authorization':'Bearer ' + String(authTokens.access)
            
          },
          body: JSON.stringify({title,description,deadline}) 
        });
        const task = await response.json();
        
        if(response.status === 200){
            setTasks(tasks.concat(task))
        }else {
            logoutUser()
        }
      }
      
      const deleteTask =async (id) =>{
        /* 
            Deleting tasks of a user
        */
        const response = await fetch(`${host}/task/task-delete/${id}/`, {
          method: 'DELETE', 
          headers: {
            'Content-Type': 'application/json',
            'Authorization':'Bearer ' + String(authTokens.access)
            
          },
        });
        const task = response.json(); 
        const newTasks = tasks.filter((task)=>{return task.id!==id})
       
        if(response.status === 200){
        setTasks(newTasks);
            
        }else{
            logoutUser()
        }
      }

      const editTask = async (id,title,description) =>{
        // edit task of a user
        const response = await fetch(`${host}/task/task-update/${id}/`, {
          method: 'PUT', 
          headers: {
            'Content-Type': 'application/json',
            'Authorization':'Bearer ' + String(authTokens.access)
            
          },
          body: JSON.stringify({title,description}) 
        });
        const data = response.json(); 
        console.log(data);
      

        //logic to edit in client
        let newTasks = JSON.parse(JSON.stringify(tasks))
        for (let index = 0; index < newTasks.length; index++) {
          const element = newTasks[index];
          if(element.id===id)
          {
            newTasks[index].title=title;
            newTasks[index].description=description;
            break;
          }
        }
        
        if(response.status === 200){
            setTasks(newTasks);
        }else{
            logoutUser()
        }
      }

      const getUserData = async () =>{
        /* 
            Fetching data of current user
        */
        let response = await fetch(`${host}/auth/user-userdetails/`, {
            method:'GET',
            headers:{
                'Content-Type':'application/json',
                'Authorization':'Bearer ' + String(authTokens.access)
            }
        })
        let data = await response.json()

        if(response.status === 200){
            setUserDetails(data)
        }else if(response.statusText === 'Unauthorized'){
            logoutUser()
        }
      }

      const approveTask = async (id,taskstatus) =>{
        //approving task of users
        const response = await fetch(`${host}/task/task-approve/${id}/`, {
          method: 'PUT', 
          headers: {
            'Content-Type': 'application/json',
            'Authorization':'Bearer ' + String(authTokens.access)
            
          },
          body: JSON.stringify({taskstatus}) 
        });
        
        //logic to edit in client
        let newTasks = JSON.parse(JSON.stringify(tasks))
        for (let index = 0; index < newTasks.length; index++) {
            const element = newTasks[index];
            if(element.id===id)
            {
            newTasks[index].taskstatus=taskstatus;
            break;
            }
        }
                
        if(response.status === 200){
            setTasks(newTasks);
        }else{
            logoutUser()
        }
      }

    return(
       <TaskContext.Provider value={{tasks,addTask,deleteTask,editTask,getTask,getAllTask,getDependentTask,approveTask,userDetails,getUserData}}>
           {props.children}
       </TaskContext.Provider>

    );
}

export default TaskState;