import React from 'react'

function TaskDetail(props) {

  return (
    <div>
      <div>
        <div className="modal fade" id="DetailModal" tabIndex={-1} role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div className="modal-dialog modal-lg" role="document">
            <div className="modal-content">
              <div className="modal-body">
                <div className="row">
                  <div className="col-lg-7">
                    <h2 className="h2-responsive product-name">
                      <strong>{props.task.etitle}</strong>
                    </h2>
                    <div className="py-2 px-3 me-2 border border-warning rounded-3 d-flex align-items-center bg-light ">
                      <p className="small mb-0">
                      <a title="Due on date">
                          <i className="fas fa-hourglass-half me-2 text-warning" />
                      </a>
                      {props.task.edeadline}
                      </p>
                    </div>
                    <div className="accordion md-accordion" id="accordionEx" role="tablist" aria-multiselectable="true">
                      <div className="card">
                        <div id="collapseOne1" className="collapse show" role="tabpanel" aria-labelledby="headingOne1" data-parent="#accordionEx">
                          <div className="card-body">
                            {props.task.edescription}
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="modal-footer">
                      <button
                        type="button"
                        className="btn btn-secondary"
                        data-bs-dismiss="modal"
                      >
                        Close
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default TaskDetail;
