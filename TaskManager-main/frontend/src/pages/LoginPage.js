import React, {useContext} from 'react'
import AuthContext from '../context/AuthContext'

const LoginPage = () => {
    let {loginUser} = useContext(AuthContext)
    return (
<div className="container mt-3">
      <h2>Login to Taskcrew</h2>
      <form onSubmit={loginUser}>
        <div className="mb-3">
          <label htmlFor="Email" className="form-label">
            Username
          </label>
          <input
            type="text"
            className="form-control"
            id="username"
            name="username"
          />
        </div>
        <div className="mb-3">
          <label htmlFor="Password" className="form-label">
            Password
          </label>
          <input
            type="password"
            className="form-control"
            id="Password"
            name="password"
          />
        </div>
        <button type="submit"  className="btn btn-primary">
          Submit
        </button>
      </form>
    </div>
    )
}

export default LoginPage
