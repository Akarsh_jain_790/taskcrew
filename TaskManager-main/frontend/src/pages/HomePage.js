import React, {useContext} from 'react'
import TaskContext from '../context/TaskContext'

import Task from '../components/Task'
import AllTask from '../components/AllTask'

const HomePage = () => {

    let {userDetails} = useContext(TaskContext)

    return (
        <div>
            {userDetails.role === "ADMIN"?
            <AllTask/>
            :<Task/>}
        </div>
    )
}

export default HomePage
